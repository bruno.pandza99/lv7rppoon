﻿using System;

namespace ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] numberArray = new double[15];
            Random randomGenerator = new Random();
            for (int i = 0; i < 15; ++i)
            {
                numberArray[i] = randomGenerator.Next(1, 20);
            }
            NumberSequence array = new NumberSequence(numberArray);
            for (int i = 0; i < 15; ++i)
            {
                Console.Write(numberArray[i] + "    ");
            }
            double wantedNumber = 10;
            Console.WriteLine("\n\n Linear Search:");
            Console.WriteLine("We are looking for this number -  " + wantedNumber);
            LinearSearch linearSearch = new LinearSearch();
            array.SetSearchStrategy(linearSearch);
            double wantedNumberPlace = array.Search(wantedNumber);
            if (wantedNumberPlace != -1)
            {
                Console.WriteLine("The number was found in " + (wantedNumberPlace + 1) + ". place in array");
            }
            else
            {
                Console.WriteLine("Number " + wantedNumber + " was not found");
            }
            Console.WriteLine("\n\nBubblesort:");
            BubbleSort bubblesort = new BubbleSort();
            array.SetSortStrategy(bubblesort);
            array.Sort();
            Console.WriteLine(array);
            Console.WriteLine("\n");

            Console.WriteLine("\n\n Binary Search:");
            Console.WriteLine("We are looking for this number -  " + wantedNumber);
            BinarySearch binarySearch = new BinarySearch();
            array.SetSearchStrategy(binarySearch);
            wantedNumberPlace = array.Search(wantedNumber);
            if (wantedNumberPlace != -1)
            {
                Console.WriteLine("The number was found in " + (wantedNumberPlace + 1) + ". place in array");
            }
            else
            {
                Console.WriteLine("Number " + wantedNumber + " was not found");
            }

        }
    }
}
