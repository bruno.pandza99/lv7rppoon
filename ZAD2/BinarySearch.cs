﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD2
{
    class BinarySearch : SearchStrategy
    {
        public override double Search(double[] array, double wantedNumber)
        {
           
            int lowerLimit = 0;
            int upperLimit = array.Length -1;

            while (lowerLimit <= upperLimit)
            {
                int middle = (lowerLimit + upperLimit) / 2;
                if (wantedNumber == array[middle])
                {
                    return middle;
                }
                else if (wantedNumber < array[middle])
                {
                    upperLimit = middle - 1;

                }
                else
                {
                    lowerLimit = middle + 1;
                }
            }
            return -1;
        }
    }
}
