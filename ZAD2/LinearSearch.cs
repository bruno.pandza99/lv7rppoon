﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD2
{
    class LinearSearch : SearchStrategy
    {
       public override double Search(double[] array, double wantedNumber)
       {
            for( int i = 0; i < array.Length; ++i)
            {
                if (wantedNumber == array[i])
                {
                    return i;
                    
                }
            }
            return -1;
        }
    }
}
