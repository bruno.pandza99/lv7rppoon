﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD2
{
    abstract class SearchStrategy
    {
        public abstract double Search(double[] array, double wantedNumber);
    }
}
