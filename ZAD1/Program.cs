﻿using System;

namespace ZAD1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] numberArray = new double [15];
            Random randomGenerator = new Random();
            for(int i = 0; i < 15; ++i)
            {
                numberArray[i] = randomGenerator.Next(1,20);
            }
            NumberSequence array = new NumberSequence(numberArray);
            for (int i = 0; i < 15; ++i)
            {
                Console.Write(numberArray[i] + "    ");
            }

            Console.WriteLine("\n\nBubblesort:");
            BubbleSort bubblesort = new BubbleSort();
            array.SetSortStrategy(bubblesort);
            array.Sort();
            Console.WriteLine(array);
            Console.WriteLine("\n");

            Console.WriteLine("\n\nCombsort:");
            CombSort combsort = new CombSort();
            array.SetSortStrategy(combsort);
            array.Sort();
            Console.WriteLine(array);
            Console.WriteLine("\n");

            Console.WriteLine("\n\nSequential sort:");
            SequentialSort sequentialSort = new SequentialSort();
            array.SetSortStrategy(sequentialSort);
            array.Sort();
            Console.WriteLine(array);
            Console.WriteLine("\n");

        }
    }
}
